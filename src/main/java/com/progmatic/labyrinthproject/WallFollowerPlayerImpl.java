/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject;


import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;

/**
 *
 * @author progmatic
 */
public class WallFollowerPlayerImpl implements Player {

    Direction facing = Direction.NORTH;

    @Override
    public Direction nextMove(Labyrinth l) {
        while (true) {
            if (l.possibleMoves().contains(getLeftDirection(facing))) {
                facing = getLeftDirection(facing);
                return facing;
            } else {
                turnRight();
            }
        }
    }

    private Direction getLeftDirection(Direction d) {
        switch (d) {
            case NORTH:
                return Direction.WEST;
            case SOUTH:
                return Direction.EAST;
            case EAST:
                return Direction.NORTH;
            default:
                return Direction.SOUTH;
        }
    }

    private void turnRight() {
        for (int i = 0; i < 3; i++) {
            facing = getLeftDirection(facing);
        }
    }

}
