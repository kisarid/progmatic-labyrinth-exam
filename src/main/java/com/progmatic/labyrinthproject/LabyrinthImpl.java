package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    private int width, height;
    private CellType[][] layout;
    private Coordinate playerPosition, start, end;

    public LabyrinthImpl() {
    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            width = Integer.parseInt(sc.nextLine());
            height = Integer.parseInt(sc.nextLine());
            setSize(width, height);

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            setCellType(new Coordinate(ww, hh), CellType.WALL);
                            break;
                        case 'S':
                            start = new Coordinate(ww, hh);
                            setCellType(start, CellType.START);
                            break;
                        case 'E':
                            end = new Coordinate(ww, hh);
                            setCellType(end, CellType.END);
                            break;
                    }

                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        } catch (CellException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public int getWidth() {
        if (layout == null) {
            return -1;
        }
        return width;
    }

    @Override
    public int getHeight() {
        if (layout == null) {
            return -1;
        }
        return height;
    } 

    @Override
    public CellType getCellType(Coordinate c) throws CellException {
        try {

            return layout[c.getRow()][c.getCol()];

        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new CellException(c.getRow(), c.getCol(), "Coordinates are out of bounds.");
        }
    }

    @Override
    public void setSize(int width, int height) {
        layout = new CellType[height][width];
        this.height = height;
        this.width = width;

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                layout[row][col] = CellType.EMPTY;
            }
        }
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        try {

            layout[c.getRow()][c.getCol()] = type;
            if (type.equals(CellType.START)) {
                playerPosition = c;
            }

        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new CellException(c.getRow(), c.getCol(), "Coordinates are out of bounds.");
        }
    }

    @Override
    public Coordinate getPlayerPosition() {
        return playerPosition;
    }

    @Override
    public boolean hasPlayerFinished() {
        try {

            return getCellType(playerPosition) == CellType.END;

        } catch (CellException ex) {
            throw new RuntimeException("Invalid player position");
        }
    }

    @Override
    public List<Direction> possibleMoves() {
        List<Direction> result = new ArrayList<>();

        for (Direction direction : Direction.values()) {
            try {
                if (!getCellType(direction.move(playerPosition)).equals(CellType.WALL)) {
                    result.add(direction);
                }
            } catch (CellException ex) {
                // checked a cell out of bounds
            }
        }

        return result;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {
        if (!possibleMoves().contains(direction)) {
            throw new InvalidMoveException();
        }

        playerPosition = direction.move(playerPosition);
    }

}
