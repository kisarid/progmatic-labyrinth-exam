package com.progmatic.labyrinthproject.enums;

import com.progmatic.labyrinthproject.Coordinate;

/**
 *
 * @author pappgergely
 */
public enum Direction {

    NORTH {
        @Override
        public Coordinate move(Coordinate c) {
            return new Coordinate(c.getCol(), c.getRow() - 1);
        }
    },
    
    EAST {
        @Override
        public Coordinate move(Coordinate c) {
            return new Coordinate(c.getCol() + 1, c.getRow());
        }
    },
    
    SOUTH {
        @Override
        public Coordinate move(Coordinate c) {
            return new Coordinate(c.getCol(), c.getRow() + 1);
        }
    },
    
    WEST {
        @Override
        public Coordinate move(Coordinate c) {
            return new Coordinate(c.getCol() - 1, c.getRow());
        }
    };

    public abstract Coordinate move(Coordinate c);
}
