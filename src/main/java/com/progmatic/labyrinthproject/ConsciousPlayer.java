/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import com.progmatic.labyrinthproject.interfaces.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author progmatic
 */
public class ConsciousPlayer implements Player {

    private Coordinate start, end;
    private Map<Coordinate, Integer> mappedLabyrinth = new HashMap<>();
    private Stack<Direction> exitRoute = new Stack<>();

    private void exploreLabyrinth(Labyrinth l, int currentStep) {
        Coordinate currentCoordinate = l.getPlayerPosition();

        mappedLabyrinth.put(currentCoordinate, currentStep);

        try {
            if (l.getCellType(currentCoordinate).equals(CellType.END)) {
                end = currentCoordinate;
                return;
            }
        } catch (CellException ex) {
            System.out.println("Invalid cell in ConsciousPlayer.exploreLabyrinth()");
        }

        currentStep++;
        for (Direction possibleMove : l.possibleMoves()) {
            Coordinate nextCell = possibleMove.move(currentCoordinate);
            if (mappedLabyrinth.containsKey(nextCell)) {
                if (mappedLabyrinth.get(nextCell) > currentStep) {
                    mappedLabyrinth.put(nextCell, currentStep);
                }
            } else {
                try {
                    l.movePlayer(possibleMove);
                    exploreLabyrinth(l, currentStep);
                } catch (InvalidMoveException ex) {
                    System.out.println("Error in Labyrinth.possibleMoves(): " + possibleMove + " - " + currentCoordinate.getRow() + ", " + currentCoordinate.getCol());
                }
            }
        }
    }

    private void trackBack(Labyrinth l) {
        while (!l.getPlayerPosition().equals(start)) {
            int min = Integer.MAX_VALUE;
            Direction nextBackwardsMove = null;
            for (Direction possibleMove : l.possibleMoves()) {
                int stepsUntilPossibleMove = mappedLabyrinth.get(possibleMove.move(l.getPlayerPosition()));
                if (stepsUntilPossibleMove < min) {
                    min = stepsUntilPossibleMove;
                    nextBackwardsMove = possibleMove;
                }
            }

            try {
                exitRoute.push(getOpposite(nextBackwardsMove));
                l.movePlayer(nextBackwardsMove);
            } catch (InvalidMoveException ex) {
                System.out.println("Invalid move in ConsciousPlayer.trackBack()");
            }
        }
    }

    private Direction getOpposite(Direction d) {
        switch (d) {
            case NORTH:
                return Direction.SOUTH;
            case SOUTH:
                return Direction.NORTH;
            case EAST:
                return Direction.WEST;
            default:
                return Direction.EAST;
        }
    }

    @Override
    public Direction nextMove(Labyrinth l) {
        if (mappedLabyrinth.isEmpty()) {
            start = l.getPlayerPosition();
            exploreLabyrinth(l, 0);
            trackBack(l);
        }

        if(exitRoute.isEmpty()){
            return null;
        }
        
        return exitRoute.pop();
    }

}
